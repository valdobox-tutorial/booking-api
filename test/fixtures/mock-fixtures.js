const swagger = require('swagger-express-middleware')
const MemoryDataStore = swagger.MemoryDataStore
const Resource = swagger.Resource

const mockMemoryDB = new MemoryDataStore()

const bookingFixtures = [{
  id: 'testId1',
  holidayId: 'htestId1',
  customerId: 'ctestId1',
  price: 600,
  startDate: '2020-06-19T09:12:51.205Z',
  endDate: '2020-06-29T09:12:51.205Z'
}, {
  id: 'testId2',
  holidayId: 'htestId2',
  customerId: 'ctestId2',
  price: 200,
  startDate: '2020-07-15T09:12:51.205Z',
  endDate: '2020-07-18T09:12:51.205Z'
}]

mockMemoryDB.save(
  new Resource('/booking-api/bookings/testId1', bookingFixtures[0]),
  new Resource('/booking-api/bookings/testId2', bookingFixtures[1])
)

module.exports = {
  mockMemoryDB
}
